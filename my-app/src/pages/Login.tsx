import React, { FC } from 'react';
import { TextField, Button} from '@mui/material';
import { useForm, Controller, SubmitHandler } from "react-hook-form"
import axios from 'axios';

interface Props {
  message: string;
}

const Login: FC<Props> = ({ message }) => {
const { control, handleSubmit } = useForm({
  })

  const onSubmit  = async (data:any) => {
    console.log(data)
    await axios({
        method: 'post',
        url: 'http://peter-nuc.taile9957.ts.net:10000/user/login',
        data: {
          username: data.userName,
          password: data.password
        }
      });
  }
return(
  <div className="login-header-container">
    <span>{message}</span>
    <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
            name="userName"
            control={control}
            render={({ field }) => <TextField id="filled-basic" label="User name" variant="filled" {...field}/>}
        />
        <Controller
            name="password"
            control={control}
            render={({ field }) => <TextField id="filled-basic" label="Password" variant="filled" {...field}/>}
        />
        <Button variant="contained" type="submit">Submit</Button>
    </form>
  </div>
);
}
export default Login;