import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './pages/Login';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
    <Routes>
        <Route path="login" element={<Login message='Halo halo.'/>} />
    </Routes>
    </BrowserRouter>
  );
}

export default App;
